<?php

/**
 * @file
 * The submission emailing system to inform the appropriate recipients
 * 
 */

/**
 * Function to call Drupal's email function when a TPR is submitted.
 * 
 *  @param array $TPS
 *    The reference variable post processing
 *  @param string $type
 *    Type of TPS action to alert
 *  @param $to
 *    Chance to add additional emial addresses to the mailing
 * 
 *  @return
 *    TODO: Return the status of the emails sent
 */
function emugetpr_submit_emails(&$TPS, $type = 'submission', $to = array('developer@emuge.com')) {
  $params = array();
  
  // Two versions of the mail to list for testing
  $test_bcc = 'rickwphillips@gmail.com, rick.phillips@emuge.com, jamie.fiffy@emuge.com';
  $live_bcc = 'rickwphillips@gmail.com, rick.phillips@emuge.com, bob.hellinger@emuge.com, marlon.blandon@emuge.com, jose.alvarenga@emuge.com, mark.hatch@emuge.com';
  
  // Set to live
  $params['headers']['Bcc'] = $test_bcc;
  $params['TPS'] = $TPS;
  
  // Send multiple emails if an array of names is passed in $to
  if (is_array($to)) {
    foreach($to as $recipient) {
      drupal_mail('emuge_tpr', $type, $recipient, user_preferred_language(1), $params, 'developer@emuge.com');
    } 
  }
  // Send single email with string in the $to value
  else {
      drupal_mail('emuge_tpr', $type, $to, user_preferred_language(1), $params, 'developer@emuge.com');
  }
  
  return;
}

/**
 *  Implements hook_mail(). 
 */
function emuge_tpr_mail($key, &$message, $params) {
  $message['headers']['Bcc'] = $params['headers']['Bcc'];
  $TPS = $params['TPS'];
  
  switch ($key) {
    case 'submission' :
      // Set subject to to the newly submitted text
      $message['subject'] = $TPS['sales_manager'] . ' has submitted a new Tool Performance Report. Results: ' . $TPS['success'];
      break;  
    
    case 'update' :
      // Set subject to to the newly updated text
      $message['subject'] = $TPS['sales_manager'] . ' has updated Tool Performance Report #' . $TPS['sid'];
      break;
    
    default :
      // No default action
  }
  
  // Add the default body to the message
  $message['body'][] = 'The report can be seen at: <a href="http://emuge.co/ec-tpr/submission/' . $TPS['sid'] . '">here</a>';
  $message['body'][] = 'Make sure to log in if you are not granted access to the reports.';
  $message['body'][] = 'This is an automated message from web.developer@emuge.com. Please do not reply to this email.';
}